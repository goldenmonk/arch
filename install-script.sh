#!/bin/bash

# Open the encrypted volume
cryptsetup luksOpen /dev/sda3 /luks
#enter passphrase

# format all the partitions
mkfs.vfat -F32 /dev/sda1
mkfs.ext2 /dev/sda2
mkfs.ext4 /dev/mapper/vg-root
#mkfs.ext4 /dev/mapper/vg-home #optional

# mount the root volume
mount /dev/mapper/vg-root /mnt

# Create home directory for separate home volume
mkdir /mnt/home

# Create boot  directory for efi boot
mkdir /mnt/boot

#mount home, boot and efi
mount /dev/mapper/vg-home /mnt/home
mount /dev/sda2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/sda1 /mnt/boot/efi


# Create swap space
cd /mnt 
dd if=/dev/zero of=/swap bs=1M count=1024
mkswap /swap
swapon /swap
chmod 0600 /swap
cd

# install using pacstrap
pacstrap /mnt intel-ucode base linux-zen linux-firmware linux-zen-headers nano sudo grub efibootmgr networkmanager network-manager-applet lvm2 base-devel gvfs ufw ntfs-3g cron gvfs-mtp git wireless_tools broadcom-wl-dkms wpa_supplicants ttf-dejavu thunar sway swaylock swayidle alacritty vlc shotwell htop neofetch papirus-icon-theme homebank telegram-desktop libreoffice-still xorg-xwayland ttf-font-awesome  brightnessctl pulseaudio dialog pulseaudio-alsa alsa-utils

# Create fstab
genfstab -U /mnt >> /mnt/etc/fstab
# "/swapfile none swap defaults 0 0" >> /mnt/etc/fstab

arch-chroot /mnt

#Edit /etc/mkinitcpio.conf
#HOOKS="base udev autodetect modconf block keymap encrypt lvm2 resume filesystems keyboard fsck"
# mkinitcpio -p linux-zen

# Set timezone
#timedatectl set-timezone America/Los_Angeles
timedatectl set-timezone Asia/Kolkata
hwclock --systohc

# Set locale 
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8

# Set hosts
echo myarch > /etc/hostname
touch /etc/hosts
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1 localhost" >> /etc/hosts
echo "127.0.1.1 myarch" >> /etc/hosts

 #nano /etc/default/grub
#GRUB_CMDLINE_LINUX=”cryptdevice=/dev/sda3:vg-root:allow-discards”

# Install Grub 
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
grub-mkconfig -o /boot/grub/grub.cfg

# enable services
systemctl enable ufw.service
systemctl enable NetworkManager.service

passwd
#useradd -m -G wheel,storage username
#passwd username

sudo EDITOR=nano visudo

# Multilib support
# Uncomment this section
# [multilib]
# Include = /etc/pacman.d/mirrorlist
echo "[multilib]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf
pacman -S multilib-devel

# Install Halium specific packages
pacman -S repo 

# Install yay helper 
cd /opt
sudo git clone https://aur.archlinux.org/yay-git.git
sudo chown -R maverick ./yay-git
cd yay-git
makepkg -si

yay -S brave-bin #Brave browser
yay -S timeshift-bin #Backup utility
yay -S stacer # System utility
yay -S gammastep waybar  vscodium-bin wofi thunderbird
echo "exec gammastep -P -O 2500" >> ~/.config/sway/config
echo "exec brave --enable-features=UseOzonePlatform --ozone-platform=wayland" >> ~/.config/sway/config
#alsa-firmware alsa-utils alsa-plugins pulseaudio-alsa pulseaudio
 #exec --no-startup-id "pulseaudio --start
echo "bindsym XF86MonBrightnessUp exec --no-startup-id xbacklight -inc 5 # increase screen brightness" >> ~/.config/sway/config
echo "bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 5 # decrease screen brightness" >> ~/.config/sway/config
